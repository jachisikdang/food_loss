git pull
export JAVA_HOME=/usr/lib/jvm/zulu11.35.13-ca-jdk11.0.5-linux_x64/
./gradlew build -x test
docker build -t nanwo .
docker run -d -p 7000:7000 nanwo
docker rmi $(docker images | grep "none" | awk '/ / { print $3 }')
