package com.foodloss.v1.order.request.service;

import java.util.Optional;
import javax.transaction.Transactional;
import com.foodloss.v1.order.request.model.entity.Order;
import com.foodloss.v1.order.request.repository.OrderRepository;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Developer : 김태현
 * Date : 10/10/2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceImplTest {

    private final Long memberId = 1L;
    private final Long itemId = 1L;
    private final int count = 2;
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderRepository orderRepository;


    @Test
    @Transactional
    public void 주문생성_성공() {
        // given

        // when
        Long orderId = orderService.order(memberId, itemId, count);

        Optional<Order> orderOptional = orderRepository.findById(orderId);

        // then
        assertTrue(orderOptional.isPresent());

    }

}
