package com.foodloss.common.info;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ServiceInfo {

    public static final String SERVICE_NAME = "CUBE_GOODS";
    public static final String VERSION = "v1";
    public static final String URI_PATH_PREFIX = "/" + VERSION;

    public static String PROFILE;


    @Value("${spring.profiles.active}")
    private void setProfiles(String profile) {
        PROFILE = profile;
    }
}
