package com.foodloss.common.slack;

import java.io.IOException;
import javax.annotation.PostConstruct;
import com.ullink.slack.simpleslackapi.SlackChannel;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SlackClient {

    @Value("${slack.token}")
    private String auth_token;

    private SlackSession slackSession;


    public void sendRawMessageToChannel(final Channel channel, final String message) {
        if(slackSession.isConnected()) {
            final SlackChannel slackChannel = slackSession.findChannelByName(channel.name());
            slackSession.sendMessage(slackChannel, message);
        }
    }


    @PostConstruct
    public void postConstruct() {
        slackSession = SlackSessionFactory.createWebSocketSlackSession(auth_token);
        try {
            slackSession.connect();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }


    public enum Channel {
        주문접수, 에러로그
    }
}
