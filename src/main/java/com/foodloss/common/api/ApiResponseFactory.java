package com.foodloss.common.api;

import java.io.IOException;
import com.foodloss.common.info.ServiceInfo;
import com.foodloss.common.util.JsonUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import retrofit2.Response;

/**
 * Developer : 김태현
 * Date : 2019-08-22
 */
public class ApiResponseFactory {

    private ApiResponseFactory() {

    }


    public static ResponseEntity<ApiResponse> createResponse(Response<ApiResponse> response)
            throws IOException {
        ApiResponse apiResponse = !response.isSuccessful() ?
                (ApiResponse) JsonUtil
                        .jsonStringToObject(response.errorBody().string(), ApiResponse.class) :
                response.body();
        return new ResponseEntity<>(apiResponse, HttpStatus.valueOf(response.code()));
    }


    public static ResponseEntity<ApiResponse> createSuccessResponse(String requestUri,
            String message) {
        return createSuccessResponse(requestUri, message, null, 1);
    }


    public static ResponseEntity<ApiResponse> createSuccessResponse(String requestUri,
            String message, Object data) {
        return createSuccessResponse(requestUri, message, data, 1, HttpStatus.OK);
    }


    public static ResponseEntity<ApiResponse> createSuccessResponse(String requestUri,
            String message, Object data, int count) {
        return createSuccessResponse(requestUri, message, data, count, HttpStatus.OK);
    }


    public static ResponseEntity<ApiResponse> createSuccessResponse(String requestUri,
            String message, Object data, int count, HttpStatus httpStatus) {
        return new ResponseEntity<>(
                new ApiResponse.Builder(ServiceInfo.VERSION, requestUri).message(message).data(data)
                        .count(count).build(), httpStatus);
    }


    public static ResponseEntity<ApiResponse> createFailResponse(String requestUri,
            ApiErrorCode errorCode, String errorMessage, HttpStatus httpStatusCode) {

        return new ResponseEntity<>(new ApiResponse.Builder(ServiceInfo.VERSION, requestUri,
                ApiError.builder().errorCode(errorCode).message(errorMessage)
                        .httpStatusCode(httpStatusCode).build()).build(), httpStatusCode);
    }
}
