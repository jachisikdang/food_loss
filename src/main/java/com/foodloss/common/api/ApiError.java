package com.foodloss.common.api;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

/**
 * API 호출 결과가 실패인 경우
 * Developer : 김태현
 * Date : 2019-08-21
 */
@Getter
@Setter
@ToString
@Builder
public class ApiError {

    private ApiErrorCode errorCode;

    private String message;

    private HttpStatus httpStatusCode;
}
