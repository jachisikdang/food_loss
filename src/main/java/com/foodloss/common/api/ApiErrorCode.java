package com.foodloss.common.api;

import com.fasterxml.jackson.annotation.JsonValue;
import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;

/**
 * Developer : 김태현
 * Date : 2019-08-21
 */
@AllArgsConstructor
public enum ApiErrorCode {

    @SerializedName("000") SUCCESS("000", "success"),
    @SerializedName("100") PAY_IAMPORT_PAYMENT_FAIL("100", "iampoort paymennt fail");


    @JsonValue
    private String code;
    private String messageKey;
}
