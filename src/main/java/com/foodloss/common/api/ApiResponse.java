package com.foodloss.common.api;

import java.io.IOException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.foodloss.common.util.JsonUtil;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import retrofit2.Response;

@Getter
@Setter
public class ApiResponse {

    private Object data;

    private int count;

    private String version;

    @SerializedName("uri")
    private String requestUri;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ApiError error;

    private String message;


    public ApiResponse(ApiResponse.Builder builder) {
        this.version = builder.version;
        this.requestUri = builder.requestUri;
        this.data = builder.data;
        this.count = builder.count;
        this.error = builder.error;
        this.message = builder.message;
    }


    public static ApiResponse getApiResponse(Response<ApiResponse> response) throws IOException {
        return !response.isSuccessful() ?
                (ApiResponse) JsonUtil
                        .jsonStringToObject(response.errorBody().string(), ApiResponse.class) :
                (ApiResponse) response.body();
    }


    public static class Builder {

        private final String version;
        private final String requestUri;
        private String message = "";
        private Object data = null;
        private ApiError error;
        private int count = 0;


        public Builder(String version, String requestUri) {
            this.version = version;
            this.requestUri = requestUri;
        }


        public Builder(String version, String requestUri, ApiError apiError) {
            this.version = version;
            this.requestUri = requestUri;
            this.error = apiError;

        }


        public ApiResponse.Builder message(String message) {
            this.message = message;
            return this;
        }


        public ApiResponse.Builder data(Object data) {
            this.data = data;
            return this;
        }


        public ApiResponse.Builder count(int count) {
            this.count = count;
            return this;
        }


        public ApiResponse build() {
            return new ApiResponse(this);
        }
    }
}
