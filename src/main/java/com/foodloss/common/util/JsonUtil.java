package com.foodloss.common.util;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import com.foodloss.common.exception.InvalidArgumentException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;
import com.google.gson.reflect.TypeToken;

public class JsonUtil {

    private static Gson defaultConverter;
    private static Gson exposeConverter;

    static {
        setDefaultConverter();
        setExposeConverter();
    }


    public JsonUtil() {
    }


    private static void setDefaultConverter() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new DateTypeAdapter());
        defaultConverter = builder.setPrettyPrinting().create();
    }


    private static void setExposeConverter() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new DateTypeAdapter());
        builder.excludeFieldsWithoutExposeAnnotation();
        exposeConverter = builder.setPrettyPrinting().create();
    }


    public static Gson getConverter(JsonUtil.Mode mode) throws InvalidArgumentException {
        if(JsonUtil.Mode.DEFAULT == mode) {
            return defaultConverter;
        } else if(JsonUtil.Mode.EXPOSE == mode) {
            return exposeConverter;
        } else {
            throw new InvalidArgumentException("Invalid converter mode.");
        }
    }


    public static String objectToJsonString(Object object) {
        return objectToJsonString(object, JsonUtil.Mode.DEFAULT);
    }


    public static String objectToJsonString(Object object, JsonUtil.Mode mode) {
        try {
            return getConverter(mode).toJson(object);
        } catch(InvalidArgumentException var3) {
            var3.printStackTrace();
            return var3.getMessage();
        }
    }


    public static <T> T jsonStringToObject(String jsonString, Type type) {
        return jsonStringToObject(jsonString, type, JsonUtil.Mode.DEFAULT);
    }


    public static <T> T jsonStringToObject(String jsonString, Type type, JsonUtil.Mode mode) {
        try {
            return getConverter(mode).fromJson(jsonString, type);
        } catch(InvalidArgumentException var4) {
            var4.printStackTrace();
            return null;
        }
    }


    public static <T> T objectToObject(Object object, Type type) {
        return objectToObject(object, type, JsonUtil.Mode.DEFAULT);
    }


    public static <T> T objectToObject(Object object, Type type, JsonUtil.Mode mode) {
        try {
            return getConverter(mode).fromJson(getConverter(mode).toJson(object), type);
        } catch(InvalidArgumentException var4) {
            var4.printStackTrace();
            return null;
        }
    }


    public static <T> List<T> listToObjects(List objects, Type type) {
        return listToObjects(objects, type, JsonUtil.Mode.DEFAULT);
    }


    public static <T> List<T> listToObjects(List objects, Type type, JsonUtil.Mode mode) {
        Type listType = TypeToken.getParameterized(List.class, new Type[] {type}).getType();

        try {
            return (List) getConverter(mode).fromJson(getConverter(mode).toJson(objects), listType);
        } catch(InvalidArgumentException var5) {
            var5.printStackTrace();
            return null;
        }
    }


    public static <T> List<T> jsonStringToObjects(String jsonString, Type type) {
        return jsonStringToObjects(jsonString, type, JsonUtil.Mode.DEFAULT);
    }


    public static <T> List<T> jsonStringToObjects(String jsonString, Type type,
            JsonUtil.Mode mode) {
        Type listType = TypeToken.getParameterized(List.class, new Type[] {type}).getType();

        try {
            return (List) getConverter(mode).fromJson(jsonString, listType);
        } catch(InvalidArgumentException var5) {
            var5.printStackTrace();
            return null;
        }
    }


    public static enum Mode {
        DEFAULT, EXPOSE;


        private Mode() {
        }
    }
}
