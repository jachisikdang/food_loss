package com.foodloss.common.util;

import javax.servlet.http.HttpServletRequest;

public class CookieUtil {

    public static String getCookiePath(HttpServletRequest request) {
        String contextPath = request.getContextPath();
        return contextPath.length() > 0 ? contextPath : "/";
    }
}
