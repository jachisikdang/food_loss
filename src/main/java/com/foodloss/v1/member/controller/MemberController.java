package com.foodloss.v1.member.controller;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.foodloss.common.api.ApiResponse;
import com.foodloss.common.api.ApiResponseFactory;
import com.foodloss.v1.auth.handler.LoginSuccessHandler;
import com.foodloss.v1.auth.model.AuthenticationException;
import com.foodloss.v1.auth.model.AuthenticationResponse;
import com.foodloss.v1.auth.model.CurrentUser;
import com.foodloss.v1.auth.model.LoginRequest;
import com.foodloss.v1.auth.model.NanwoUserDetail;
import com.foodloss.v1.member.model.MemberDTO;
import com.foodloss.v1.member.service.MemberService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping(("/v1/members"))
public class MemberController {

    private final MemberService memberService;

    private final LoginSuccessHandler loginSuccessHandler;

    private final AuthenticationManager authenticationManager;


    // 회원 등록
    @PostMapping
    public ResponseEntity<ApiResponse> register(@RequestBody MemberDTO memberDTO,
            HttpServletRequest request, HttpServletResponse response) {

        log.info("회원가입 : " + memberDTO.toString());


        // 1. authenticate user
        LoginRequest loginRequest = LoginRequest.builder().username(memberDTO.getUsername())
                .password(memberDTO.getPassword()).rememberMe(false).build();

        // 2. register member
        memberService.register(memberDTO);

        authenticate(loginRequest.getUsername(), loginRequest.getPassword());

        // 3, 로그인 처리
        AuthenticationResponse authenticationResponse =
                loginSuccessHandler.loginSucceed(loginRequest, request, response);

        // 4. return response
        return ApiResponseFactory
                .createSuccessResponse(request.getRequestURI(), "회원가입 성공", authenticationResponse);
    }


    // 회원 전체 조회 Or 특정 회원 조회
    @GetMapping
    public ResponseEntity<ApiResponse> list(
            @RequestParam(required = false) Optional<String> username, HttpServletRequest request) {
        log.info("/v1/members");

        List<MemberDTO> members =
                username.map(email -> Collections.singletonList(memberService.findMember(email)))
                        .orElse(memberService.findMembers());

        return ApiResponseFactory
                .createSuccessResponse(request.getRequestURI(), "회원 조회 성공", members,
                        members.size());
    }


    // 로그인된 회원 정보
    @GetMapping("/active")
    public ResponseEntity<ApiResponse> getLoginMember(@CurrentUser NanwoUserDetail nanwoUserDetail,
            HttpServletRequest request) {
        log.info("/v1/members/active : {}", nanwoUserDetail.getUsername());

        return ApiResponseFactory.createSuccessResponse(request.getRequestURI(), "로그인한 회원 검색 성공",
                nanwoUserDetail.getMember());

    }


    /**
     * Admin 에서 총 회원 수 조회
     *
     * @param request
     * @return 가입한 회원 수
     */
    @GetMapping("/count")
    public ResponseEntity<ApiResponse> getTotalMemberCount(HttpServletRequest request) {
        return ApiResponseFactory.createSuccessResponse(request.getRequestURI(), "로그인한 회원 검색 성공",
                memberService.totalMemberCount());

    }


    private void authenticate(String username, String password) {

        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch(DisabledException e) {
            throw new AuthenticationException("USER_DISABLED", e);
        } catch(BadCredentialsException e) {
            throw new AuthenticationException("INVALID_CREDENTIALS", e);
        }
    }

}
