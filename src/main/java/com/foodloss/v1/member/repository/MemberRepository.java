package com.foodloss.v1.member.repository;

import java.util.Optional;
import com.foodloss.v1.member.model.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends JpaRepository<Member, Long> {

    Member findByIdx(Long memIdx);

    Optional<Member> findByUsername(String username);
}
