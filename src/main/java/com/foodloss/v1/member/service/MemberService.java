package com.foodloss.v1.member.service;

import java.util.List;
import com.foodloss.v1.member.model.MemberDTO;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
public interface MemberService {

    Long register(MemberDTO member);

    List<MemberDTO> findMembers();

    long totalMemberCount();

    MemberDTO findMember(String username);
}
