package com.foodloss.v1.member.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import com.foodloss.v1.member.model.Member;
import com.foodloss.v1.member.model.MemberConverter;
import com.foodloss.v1.member.model.MemberDTO;
import com.foodloss.v1.member.repository.MemberRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
@Service
@AllArgsConstructor
public class MemberServiceImpl implements MemberService {

    private final MemberRepository memberRepository;
    private final MemberConverter memberConverter;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;


    @Override
    public Long register(MemberDTO member) {
        validateDuplicateMember(member);

        // encode password
        member.setPassword(passwordEncoder.encode(member.getPassword()));

        // save
        Member save = memberRepository.save(modelMapper.map(member, Member.class));

        return save.getIdx();
    }


    private void validateDuplicateMember(MemberDTO member) {
        Optional<Member> optionalMember = memberRepository.findByUsername(member.getUsername());
        if(optionalMember.isPresent())
            throw new IllegalStateException("이미 존재하는 회원입니다.");
    }


    @Override
    public List<MemberDTO> findMembers() {
        List<Member> members = memberRepository.findAll();

        return members.stream().map(memberConverter::convert).collect(Collectors.toList());
    }


    @Override
    public long totalMemberCount() {
        return memberRepository.count();
    }


    @Override
    public MemberDTO findMember(String username) {
        Optional<Member> optionalMember = memberRepository.findByUsername(username);

        return optionalMember.map(memberConverter::convert).orElse(null);

    }
}
