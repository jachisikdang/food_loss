package com.foodloss.v1.member.model;

import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 주소 정보
 * ( 나중에 배송이 추가 된다면 재활용할 객체 )
 * Developer : 김태현
 * Date : 2019-09-26
 */
@Embeddable
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    private String city;
    private String street;
    private String zipcode;
}
