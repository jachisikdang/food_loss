package com.foodloss.v1.member.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.foodloss.v1.order.request.model.entity.Order;
import com.google.common.collect.Lists;
import lombok.Data;

/**
 * Developer : 김태현
 * Date : 10/10/2019
 */

@Data
@Entity
@Table(name = "member")
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MEMBER_ID")
    private Long idx;

    private String username;    // 회원 아이디 ( 이메일 )

    private String password;

    private String phoneNumber;

    private String name;    // 회원 이름

    @Embedded
    private Address address;

    @JsonBackReference
    @OneToMany(mappedBy = "member")
    private List<Order> orders = Lists.newArrayList();


    @Override
    public String toString() {
        return "Member{" + "idx=" + idx + ", name='" + username + '\'' + ", address=" + address
                + '}';
    }

}

