package com.foodloss.v1.member.model;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
@Component
public class MemberConverter implements Converter<Member, MemberDTO> {

    @Override
    public MemberDTO convert(Member source) {

        String address = source.getAddress() != null ?
                source.getAddress().getCity() + " " + source.getAddress().getStreet() + source
                        .getPhoneNumber() + source.getName() :
                "";

        return new MemberDTO(source.getIdx(), source.getUsername(), "", address,
                source.getPhoneNumber(), source.getName());
    }
}
