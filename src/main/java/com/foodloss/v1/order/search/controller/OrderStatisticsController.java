package com.foodloss.v1.order.search.controller;

import javax.servlet.http.HttpServletRequest;
import com.foodloss.common.api.ApiResponse;
import com.foodloss.common.api.ApiResponseFactory;
import com.foodloss.v1.order.search.service.OrderStatisticsService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Developer : 김태현
 * Date : 23/11/2019
 */
@RestController
@AllArgsConstructor
@RequestMapping(("/v1/orders"))
public class OrderStatisticsController {

    private final OrderStatisticsService orderStatisticsService;


    // 주문 전체 조회
    @GetMapping("/count")
    public ResponseEntity<ApiResponse> totalCountOfOrders(HttpServletRequest request) {
        return ApiResponseFactory.createSuccessResponse(request.getRequestURI(), "주문 조회 성공",
                orderStatisticsService.countOfOrders());
    }


    // 주문 전체 조회
    @GetMapping("/amount")
    public ResponseEntity<ApiResponse> totalAmountOfOrders(HttpServletRequest request) {
        return ApiResponseFactory.createSuccessResponse(request.getRequestURI(), "주문 조회 성공",
                orderStatisticsService.totalAmountOfOrders());
    }
}
