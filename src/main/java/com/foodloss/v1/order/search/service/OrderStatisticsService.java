package com.foodloss.v1.order.search.service;

/**
 * Developer : 김태현
 * Date : 23/11/2019
 */
public interface OrderStatisticsService {

    long countOfOrders();

    long totalAmountOfOrders();
}
