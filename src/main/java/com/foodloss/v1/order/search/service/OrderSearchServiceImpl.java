package com.foodloss.v1.order.search.service;

import java.util.List;
import java.util.stream.Collectors;
import com.foodloss.v1.order.request.model.dto.OrderDTO;
import com.foodloss.v1.order.request.model.entity.Order;
import com.foodloss.v1.order.request.repository.OrderRepository;
import com.foodloss.v1.order.search.model.OrderSearch;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Developer : 김태현
 * Date : 09/11/2019
 */
@Service
@AllArgsConstructor
public class OrderSearchServiceImpl implements OrderSearchService {

    private final OrderRepository orderRepository;
    private final ModelMapper modelMapper;


    @Override
    public Order findOrder(String orderId) {
        return orderRepository.findById(orderId);
    }


    @Override
    public List<OrderDTO> findOrders(Pageable pageable) {

        // 주문 내역 조회
        Page<Order> orders = orderRepository.findAll(pageable);

        // Order -> OrderDTO
        Page<OrderDTO> orderDTOPage = orders.map(order -> modelMapper.map(order, OrderDTO.class));

        // 주문 내역 리스트 생성
        List<OrderDTO> orderDTOList = Lists.newArrayList();
        orderDTOList.addAll(orderDTOPage.getContent());

        return orderDTOList;


    }


    @Override
    public List<OrderDTO> findOrders(OrderSearch orderSearch) {
        List<Order> orders = orderRepository
                .findAllByMember_UsernameAndStatus(orderSearch.getUsername(),
                        orderSearch.getOrderStatus());

        // convert to dto list
        return orders.stream().map(order -> modelMapper.map(order, OrderDTO.class))
                .collect(Collectors.toList());
    }

}
