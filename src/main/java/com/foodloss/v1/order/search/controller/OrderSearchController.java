package com.foodloss.v1.order.search.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import com.foodloss.common.api.ApiResponse;
import com.foodloss.common.api.ApiResponseFactory;
import com.foodloss.v1.auth.model.CurrentUser;
import com.foodloss.v1.auth.model.NanwoUserDetail;
import com.foodloss.v1.order.request.model.dto.OrderDTO;
import com.foodloss.v1.order.request.model.enums.OrderStatus;
import com.foodloss.v1.order.search.model.OrderSearch;
import com.foodloss.v1.order.search.service.OrderSearchService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 주문내역 검색 컨트롤러
 * <p>
 * Developer : 김태현
 * Date : 04/10/2019
 */
@RestController
@AllArgsConstructor
@RequestMapping(("/v1/orders"))
public class OrderSearchController {

    private final OrderSearchService orderSearchService;


    // 주문 전체 조회
    @GetMapping
    public ResponseEntity<ApiResponse> orderAll(@PageableDefault Pageable pageable,
            HttpServletRequest request) {

        // 주문 페이징 조회
        List<OrderDTO> orders = orderSearchService.findOrders(pageable);

        return ApiResponseFactory
                .createSuccessResponse(request.getRequestURI(), "주문 조회 성공", orders, orders.size());
    }


    // 특정 회원의 주문 목록
    @GetMapping("/member")
    public ResponseEntity<ApiResponse> getOrdersOfMember(@CurrentUser NanwoUserDetail userDetail,
            @RequestParam("status") OrderStatus status, HttpServletRequest request) {

        OrderSearch orderSearch =
                OrderSearch.builder().orderStatus(status).username(userDetail.getUsername())
                        .build();

        List<OrderDTO> orders = orderSearchService.findOrders(orderSearch);

        return ApiResponseFactory
                .createSuccessResponse(request.getRequestURI(), "회원 주문 조회 성공", orders,
                        orders.size());

    }
}
