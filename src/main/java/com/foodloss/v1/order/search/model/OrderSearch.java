package com.foodloss.v1.order.search.model;

import com.foodloss.v1.order.request.model.enums.OrderStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
@Getter
@Setter
@Builder
public class OrderSearch {

    private String username;

    private OrderStatus orderStatus;
}
