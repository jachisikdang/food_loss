package com.foodloss.v1.order.search.service;

import java.util.List;
import com.foodloss.v1.order.request.model.dto.OrderDTO;
import com.foodloss.v1.order.request.model.entity.Order;
import com.foodloss.v1.order.search.model.OrderSearch;
import org.springframework.data.domain.Pageable;

/**
 * 주문 내역 검색과 관련된 서비스
 * <p>
 * Developer : 김태현
 * Date : 09/11/2019
 */
public interface OrderSearchService {

    Order findOrder(String orderId);

    List<OrderDTO> findOrders(Pageable pageable);

    List<OrderDTO> findOrders(OrderSearch orderSearch);

}
