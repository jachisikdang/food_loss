package com.foodloss.v1.order.request.service;

import com.foodloss.v1.items.base.model.Item;
import com.foodloss.v1.items.base.repository.ItemRepository;
import com.foodloss.v1.member.model.Member;
import com.foodloss.v1.member.repository.MemberRepository;
import com.foodloss.v1.order.request.model.entity.Order;
import com.foodloss.v1.order.request.model.entity.OrderItem;
import com.foodloss.v1.order.request.model.factory.OrderFactory;
import com.foodloss.v1.order.request.model.factory.OrderItemFactory;
import com.foodloss.v1.order.request.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final MemberRepository memberRepository;
    private final ItemRepository itemRepository;


    @Override
    public String order(Long memberId, Long itemId, int count) {

        // 1. 상품, 멤버 가져오기
        Item item = itemRepository.findByIdx(itemId);

        Member member = memberRepository.findByIdx(memberId);

        // 2. 주문 상품 생성
        OrderItem orderItem = OrderItemFactory.createOrderItem(item, item.getPrice(), count);

        // 3. 주문 생성
        Order order = OrderFactory.createOrder(member, orderItem);

        // 4. 주문 저장
        orderRepository.save(order);

        return order.getId();
    }


    @Override
    public void cancel(Long orderId) {
        Order order = orderRepository.getOne(orderId);
        order.cancel();
    }
}
