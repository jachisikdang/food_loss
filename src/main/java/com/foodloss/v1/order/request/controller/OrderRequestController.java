package com.foodloss.v1.order.request.controller;

import javax.servlet.http.HttpServletRequest;
import com.foodloss.common.api.ApiResponse;
import com.foodloss.common.api.ApiResponseFactory;
import com.foodloss.v1.auth.model.CurrentUser;
import com.foodloss.v1.auth.model.NanwoUserDetail;
import com.foodloss.v1.order.request.model.dto.OrderRequestDTO;
import com.foodloss.v1.order.request.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 주문 생성 / 요청
 * Developer : 김태현
 * Date : 11/10/2019
 */
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping(("/v1/orders"))
public class OrderRequestController {

    private final OrderService orderService;


    // 주문 생성하기
    @PostMapping
    public ResponseEntity<ApiResponse> orderRequest(@RequestBody OrderRequestDTO orderRequestDTO,
            @CurrentUser NanwoUserDetail currentUser, HttpServletRequest request) {

        // logging
        log.info("/v1/orders - 주문생성 : {} , 회원 : {}", orderRequestDTO, currentUser);

        // order
        String orderId = orderService
                .order(currentUser.getMember().getIdx(), orderRequestDTO.getItemId(),
                        orderRequestDTO.getCount());
        // response
        return ApiResponseFactory
                .createSuccessResponse(request.getRequestURI(), "주문 생성 성공", orderId, 1);

    }


    // 주문 취소
    @GetMapping(value = "{orderId}/cancel")
    public ResponseEntity<ApiResponse> cancelBuy(@PathVariable("orderId") Long orderId,
            HttpServletRequest request) {

        orderService.cancel(orderId);

        return ApiResponseFactory.createSuccessResponse(request.getRequestURI(), "");
    }
}
