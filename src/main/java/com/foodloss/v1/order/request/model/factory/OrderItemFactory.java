package com.foodloss.v1.order.request.model.factory;

import com.foodloss.v1.items.base.model.Item;
import com.foodloss.v1.order.request.model.entity.OrderItem;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
public class OrderItemFactory {

    private OrderItemFactory() {

    }


    public static OrderItem createOrderItem(Item item, int orderPrice, int count) {

        OrderItem orderItem = new OrderItem();
        orderItem.setItem(item);
        orderItem.setOrderPrice(orderPrice);
        orderItem.setCount(count);

        return orderItem;
    }

}
