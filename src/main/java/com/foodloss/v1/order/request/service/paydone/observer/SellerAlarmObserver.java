package com.foodloss.v1.order.request.service.paydone.observer;

import com.foodloss.v1.order.request.model.entity.Order;
import org.springframework.stereotype.Service;

/**
 * 구매 후 판매자에게 알림 서비스
 * <p>
 * Developer : 김태현
 * Date : 12/10/2019
 */
@Service
public class SellerAlarmObserver implements PayDoneObserver {

    @Override
    public void handle(Order order) {

    }
}
