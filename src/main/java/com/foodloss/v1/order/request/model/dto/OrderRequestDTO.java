package com.foodloss.v1.order.request.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderRequestDTO {

    private Long itemId;

    private int count;

}
