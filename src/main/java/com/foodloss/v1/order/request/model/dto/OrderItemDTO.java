package com.foodloss.v1.order.request.model.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.foodloss.v1.items.base.model.ItemDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Developer : 김태현
 * Date : 11/10/2019
 */
@Getter
@Setter
@NoArgsConstructor
public class OrderItemDTO {

    private Long id;

    private ItemDTO item;      //주문 상품

    @JsonBackReference
    private OrderDTO order;    // 주문

    private int orderPrice; // 주문 가격

    private int count;  // 주문 수량


    /**
     * 주문상품 전체 가격 조회
     */
    public int getTotalPrice() {
        return getOrderPrice() * getCount();
    }
}
