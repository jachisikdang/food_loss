package com.foodloss.v1.order.request.service.paydone;

/**
 * Developer : 김태현
 * Date : 12/10/2019
 */
public interface OrderPayDoneService {

    void processAfterPaySuccess(String orderId);
}
