package com.foodloss.v1.order.request.model.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.foodloss.v1.member.model.Member;
import com.foodloss.v1.order.request.model.enums.OrderStatus;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

/**
 * 주문
 * Developer : 김태현
 * Date : 2019-09-26
 */
@Entity
@Table(name = "orders")
@Getter
@Setter
public class Order {

    @Id
    @Column(name = "order_id")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private Member member;      //주문 회원

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<OrderItem> orderItems = Lists.newArrayList();

    private LocalDate orderDate; // 주문 시간

    @Enumerated(EnumType.STRING)
    private OrderStatus status; // 주문 상태


    public BigDecimal getTotalPrice() {
        return BigDecimal.valueOf(this.orderItems.stream()
                .mapToInt(orderItem -> orderItem.getCount() * orderItem.getOrderPrice()).sum());
    }


    public void addOrderItem(OrderItem orderItem) {
        orderItems.add(orderItem);
        orderItem.setOrder(this);
    }


    public void setMember(Member member) {
        this.member = member;
        member.getOrders().add(this);
    }


    public void cancel() {

        this.setStatus(OrderStatus.CANCEL);

        orderItems.forEach(OrderItem::cancel);
    }


    /**
     * 결제까지 성공 후 주문 처리
     */
    public void finish() {

        this.setStatus(OrderStatus.PAY_FINISH);

        orderItems.forEach(orderItem -> orderItem.getItem().removeStock(orderItem.getCount()));
    }


    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", orderDate=" + orderDate + ", status=" + status + '}';
    }
}
