package com.foodloss.v1.order.request.repository;

import java.util.List;
import com.foodloss.v1.order.request.model.entity.Order;
import com.foodloss.v1.order.request.model.enums.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
public interface OrderRepository extends JpaRepository<Order, Long> {

    Order findById(String orderId);

    List<Order> findAllByMember_UsernameAndStatus(String username, OrderStatus orderStatus);

}
