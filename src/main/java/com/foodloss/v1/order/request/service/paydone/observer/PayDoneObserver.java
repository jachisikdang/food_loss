package com.foodloss.v1.order.request.service.paydone.observer;

import com.foodloss.v1.order.request.model.entity.Order;

/**
 * Developer : 김태현
 * Date : 12/10/2019
 */
public interface PayDoneObserver {

    void handle(Order order);
}
