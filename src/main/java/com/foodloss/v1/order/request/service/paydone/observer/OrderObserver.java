package com.foodloss.v1.order.request.service.paydone.observer;

import com.foodloss.v1.order.request.model.entity.Order;
import org.springframework.stereotype.Service;

/**
 * 결제 완료 후 {@link com.foodloss.v1.order.request.model.entity.Order} 상태 관리
 * Developer : 김태현
 * Date : 12/10/2019
 */
@Service
public class OrderObserver implements PayDoneObserver {

    @Override
    public void handle(Order order) {

        order.finish();
    }
}
