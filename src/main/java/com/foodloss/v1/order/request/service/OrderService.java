package com.foodloss.v1.order.request.service;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
public interface OrderService {

    String order(Long memberId, Long good, int count);

    void cancel(Long orderId);
}
