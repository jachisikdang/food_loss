package com.foodloss.v1.order.request.service.paydone;

import java.util.List;
import com.foodloss.v1.order.request.model.entity.Order;
import com.foodloss.v1.order.request.repository.OrderRepository;
import com.foodloss.v1.order.request.service.paydone.observer.PayDoneObserver;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 결제 완료후 처리
 * <p>
 * 1. 주문 상태 변경
 * 2. 판매자에게 알림
 * <p>
 * Developer : 김태현
 * Date : 12/10/2019
 */
@Service
@AllArgsConstructor
public class OrderPayDoneServiceImpl implements OrderPayDoneService {

    private final List<PayDoneObserver> payDoneObservers;
    private final OrderRepository orderRepository;


    @Override
    public void processAfterPaySuccess(String orderId) {

        // 1. Order 조회
        Order order = orderRepository.findById(orderId);

        if(order == null)
            throw new IllegalArgumentException(orderId + "는 옳바른 주문번호가 아닙니다.");

        // 2. 결제 완료 후 해야 할 일들 호출
        payDoneObservers.forEach(payDoneObserver -> payDoneObserver.handle(order));

    }
}
