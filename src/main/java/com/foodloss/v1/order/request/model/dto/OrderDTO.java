package com.foodloss.v1.order.request.model.dto;

import java.time.LocalDate;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.foodloss.v1.member.model.MemberDTO;
import com.foodloss.v1.order.request.model.enums.OrderStatus;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 주문 DTO
 * <p>
 * Developer : 김태현
 * Date : 11/10/2019
 */
@Getter
@Setter
@NoArgsConstructor
public class OrderDTO {

    private String id;

    @JsonManagedReference
    private MemberDTO member;      //주문 회원

    @JsonManagedReference
    private List<OrderItemDTO> orderItems = Lists.newArrayList();

    private LocalDate orderDate; // 주문 시간

    private OrderStatus status; // 주문 상태

    private int totalPrice;


    /**
     * 전체 주문 가격 조회
     */
    public int getTotalPrice() {
        int price = 0;
        for(OrderItemDTO orderItem : orderItems) {
            price += orderItem.getTotalPrice();
        }
        return price;
    }
}
