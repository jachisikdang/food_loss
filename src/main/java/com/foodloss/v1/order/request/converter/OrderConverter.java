package com.foodloss.v1.order.request.converter;

import com.foodloss.v1.order.request.model.dto.OrderDTO;
import com.foodloss.v1.order.request.model.entity.Order;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Developer : 김태현
 * Date : 11/10/2019
 */
@Component
public class OrderConverter implements Converter<Order, OrderDTO> {

    @Override
    public OrderDTO convert(Order source) {
        return null;
    }
}

