package com.foodloss.v1.order.request.model.enums;

/**
 * 주문 상태
 * <p>
 * Developer : 김태현
 * Date : 04/10/2019
 */
public enum OrderStatus {

    /**
     * 주문/결제 창에 들어가면 주문 생성 ( 결제 전 )
     */
    ORDER_READY,

    /**
     * 결제까지 완료된 상태
     */
    PAY_FINISH,

    /**
     * 주문 취소
     */
    CANCEL
}
