package com.foodloss.v1.order.request.model.factory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import com.foodloss.v1.member.model.Member;
import com.foodloss.v1.order.request.model.entity.Order;
import com.foodloss.v1.order.request.model.entity.OrderItem;
import com.foodloss.v1.order.request.model.enums.OrderStatus;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
public class OrderFactory {

    private OrderFactory() {

    }


    public static Order createOrder(Member member, OrderItem... orderItems) {

        Order order = new Order();
        order.setId(getOrderNumber(member.getIdx()));
        order.setMember(member);
        for(OrderItem orderItem : orderItems) {
            order.addOrderItem(orderItem);
        }
        order.setStatus(OrderStatus.ORDER_READY);
        order.setOrderDate(LocalDate.now());

        return order;
    }


    private static String getOrderNumber(Long memberIdx) {
        return DigestUtils.md5Hex(String.valueOf(memberIdx).concat(LocalDateTime.now().toString()));
    }
}
