package com.foodloss.v1.items.card.service;

import java.util.List;
import com.foodloss.v1.items.base.model.ItemDTO;
import org.springframework.data.domain.Pageable;

/**
 * Developer : 김태현
 * Date : 2019-08-22
 */
public interface ItemsCardService {

    List<ItemDTO> getAllCards(Pageable pageable);
}
