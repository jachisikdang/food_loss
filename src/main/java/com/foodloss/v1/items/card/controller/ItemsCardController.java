package com.foodloss.v1.items.card.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import com.foodloss.common.api.ApiResponse;
import com.foodloss.common.api.ApiResponseFactory;
import com.foodloss.v1.items.base.model.ItemDTO;
import com.foodloss.v1.items.card.service.ItemsCardService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Developer : 김태현
 * Date : 2019-08-22
 */
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/v1/items-card")
public class ItemsCardController {

    private ItemsCardService cardService;


    @GetMapping
    public ResponseEntity<ApiResponse> getItemCards(@PageableDefault(size = 8) Pageable pageable,
            HttpServletRequest request) {

        List<ItemDTO> goodsCards = cardService.getAllCards(pageable);

        return ApiResponseFactory
                .createSuccessResponse(request.getRequestURI(), "", goodsCards, goodsCards.size());
    }
}
