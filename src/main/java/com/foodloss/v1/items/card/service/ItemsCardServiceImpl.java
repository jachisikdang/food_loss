package com.foodloss.v1.items.card.service;

import java.util.List;
import com.foodloss.v1.items.base.model.ItemDTO;
import com.foodloss.v1.items.base.service.ItemService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Developer : 김태현
 * Date : 2019-08-22
 */
@Service
@AllArgsConstructor
public class ItemsCardServiceImpl implements ItemsCardService {

    private final ItemService itemService;


    @Override
    public List<ItemDTO> getAllCards(Pageable pageable) {

        // get items
        Page<ItemDTO> goodsBasePage = itemService.findItems(pageable);

        // TODO items -> card
        return goodsBasePage.getContent();

    }
}
