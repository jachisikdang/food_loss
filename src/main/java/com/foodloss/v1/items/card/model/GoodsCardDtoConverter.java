package com.foodloss.v1.items.card.model;

import com.foodloss.v1.items.base.model.Food;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Developer : 김태현
 * Date : 2019-08-22
 */
@Component
public class GoodsCardDtoConverter implements Converter<Food, GoodsCardDTO> {

    @Override
    public GoodsCardDTO convert(Food source) {
        return GoodsCardDTO.builder().goodsId(source.getIdx()).description(source.getDescription())
                .goodsName(source.getName()).build();
    }
}
