package com.foodloss.v1.items.card.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Developer : 김태현
 * Date : 2019-08-22
 */
@Getter
@Setter
@Builder
public class GoodsCardDTO {

    private Long goodsId;
    private String goodsName;
    private String description;

    private String titleImage;

    private int discountRate;
    private double price;
    private double rateAvg;

}
