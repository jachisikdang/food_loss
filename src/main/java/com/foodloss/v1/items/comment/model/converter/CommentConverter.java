package com.foodloss.v1.items.comment.model.converter;

import java.util.stream.Collectors;
import com.foodloss.v1.items.comment.model.dto.CommentDTO;
import com.foodloss.v1.items.comment.model.entity.Comment;
import org.springframework.stereotype.Component;

/**
 * Developer : 김태현
 * Date : 18/10/2019
 */
@Component
public class CommentConverter {

    public CommentDTO convert(Comment source, boolean isIncludeReply) {
        CommentDTO commentDTO =
                CommentDTO.builder().id(source.getId()).memberId(source.getMember().getIdx())
                        .memberName(source.getMember().getName()).message(source.getMessage())
                        .regDate(source.getRegDate()).modifyDate(source.getModifyDate())
                        .image(source.getImage()).itemsId(source.getItemsId())
                        .parentIdx(source.getParentIdx()).rating(source.getRating()).build();
        if(isIncludeReply)
            commentDTO.setReplies(
                    source.getReplies().stream().map(comment -> convert(comment, false))
                            .collect(Collectors.toList()));
        return commentDTO;
    }
}
