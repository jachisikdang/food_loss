package com.foodloss.v1.items.comment.controller;

import javax.servlet.http.HttpServletRequest;
import com.foodloss.common.api.ApiResponse;
import com.foodloss.common.api.ApiResponseFactory;
import com.foodloss.v1.items.comment.model.dto.CommentListDTO;
import com.foodloss.v1.items.comment.service.CommentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 상품 리뷰 Developer : 김태현 Date : 2019-08-17
 */
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/v1/items")
public class CommentController {

    private final CommentService commentService;


    @GetMapping("/{itemsId}/comments")
    public ResponseEntity<ApiResponse> getComments(@PathVariable(name = "itemsId") Long itemsId,
            @RequestParam(name = "includeReply", defaultValue = "true") boolean includeReply,
            Pageable pageable, HttpServletRequest request) {

        CommentListDTO commentListDTO = commentService.getComments(itemsId, pageable, includeReply);

        return ApiResponseFactory.createSuccessResponse(request.getRequestURI(), "", commentListDTO,
                commentListDTO.getComments().size());
    }

}
