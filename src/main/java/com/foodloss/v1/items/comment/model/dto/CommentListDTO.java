package com.foodloss.v1.items.comment.model.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Developer : 김태현
 * Date : 2019-08-20
 */
@Getter
@Setter
@AllArgsConstructor
public class CommentListDTO {

    private long totalSize;

    private List<CommentDTO> comments;
}

