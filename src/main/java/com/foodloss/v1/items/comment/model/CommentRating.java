package com.foodloss.v1.items.comment.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@AllArgsConstructor
public class CommentRating {

    // 리뷰 평균 점수
    private double rating;

    // 리뷰 갯수
    private long numOfComment;
}
