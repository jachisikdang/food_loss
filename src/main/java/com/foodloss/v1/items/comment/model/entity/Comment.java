package com.foodloss.v1.items.comment.model.entity;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.foodloss.v1.member.model.Member;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

@Getter
@Setter
@Entity
@Table(name = "item_comment")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "items_id")
    private Long itemsId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private Member member;      //작성 회원

    // 평점
    @Column(name = "rating")
    private Long rating;

    @Column(name = "message")
    private String message;

    @Column(name = "image")
    private String image;

    @Column(name = "user_img")
    private String userImg;

    @Column(name = "reg_date")
    @CreationTimestamp
    private LocalDateTime regDate;

    @Column(name = "modify_date")
    @UpdateTimestamp
    private LocalDateTime modifyDate;

    @Column(name = "parent_idx")
    private Long parentIdx;


    @OneToMany(mappedBy = "parentIdx", cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "id != parent_idx")
    private List<Comment> replies = Lists.newArrayList();

}
