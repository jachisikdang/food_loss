package com.foodloss.v1.items.comment.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import com.foodloss.v1.items.comment.model.CommentRating;
import com.foodloss.v1.items.comment.model.converter.CommentConverter;
import com.foodloss.v1.items.comment.model.dto.CommentDTO;
import com.foodloss.v1.items.comment.model.dto.CommentListDTO;
import com.foodloss.v1.items.comment.model.entity.Comment;
import com.foodloss.v1.items.comment.repository.CommentRepository;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Developer : 김태현 Date : 2019-08-20
 */
@Service
@AllArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final CommentConverter commentConverter;


    @Override
    public CommentListDTO getComments(Long itemsId, Pageable pageable, boolean includeReply) {
        Page<Comment> comments = findCommentsByGoodsId(itemsId, pageable);
        List<CommentDTO> commentList = Lists.newArrayList();

        if(!comments.isEmpty())
            commentList = convertCommentDTO(comments, includeReply);

        return new CommentListDTO(comments.getTotalElements(), commentList);
    }


    @Override
    public Optional<CommentRating> getRatingOfComments(Long itemsId) {
        return commentRepository.findAvgRatingByGoodsId(itemsId);
    }


    public CommentRating get(Long itemsId) {
        List<Comment> comments = commentRepository.findAllByItemsId(itemsId);

        return new CommentRating(
                comments.stream().collect(Collectors.averagingLong(Comment::getRating)),
                comments.size());
    }


    private Page<Comment> findCommentsByGoodsId(Long itemsId, Pageable pageable) {

        return commentRepository.findCommentsByGoodsId(itemsId, pageable);
    }


    private List<CommentDTO> convertCommentDTO(Page<Comment> comments, boolean includeReply) {
        return comments.stream().map(comment -> commentConverter.convert(comment, includeReply))
                .collect(Collectors.toList());
    }

}
