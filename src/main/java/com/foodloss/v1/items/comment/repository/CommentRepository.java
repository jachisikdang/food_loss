package com.foodloss.v1.items.comment.repository;

import java.util.List;
import java.util.Optional;
import com.foodloss.v1.items.comment.model.CommentRating;
import com.foodloss.v1.items.comment.model.entity.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query("SELECT new com.foodloss.v1.items.comment.model.CommentRating(AVG(c.rating),COUNT(c.rating)) FROM Comment c WHERE c.itemsId=:itemsId")
    Optional<CommentRating> findAvgRatingByGoodsId(@Param("itemsId") Long itemsId);

    List<Comment> findAllByItemsId(Long itemsId);

    @Query(value = "SELECT c FROM Comment c WHERE c.itemsId = ?1 AND c.id = c.parentIdx "
            + " ORDER BY c.regDate DESC, c.rating DESC")
    Page<Comment> findCommentsByGoodsId(Long itemsId, Pageable pageable);
}
