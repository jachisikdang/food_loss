package com.foodloss.v1.items.comment.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Builder;
import lombok.Data;

@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "parentIdx")
@Builder
public class CommentDTO implements Serializable {


    private Long id;

    private Long itemsId;

    private Long memberId;

    private String memberName;

    private String message;

    private String image;

    private Long rating;

    private List<String> commentImages = new ArrayList<>();

    private String userImg;

    private LocalDateTime regDate;

    private LocalDateTime modifyDate;

    private Long parentIdx;

    private List<CommentDTO> replies;
}
