package com.foodloss.v1.items.comment.service;

import java.util.Optional;
import com.foodloss.v1.items.comment.model.CommentRating;
import com.foodloss.v1.items.comment.model.dto.CommentListDTO;
import org.springframework.data.domain.Pageable;

/**
 * Developer : 김태현
 * Date : 2019-08-20
 */
public interface CommentService {

    CommentListDTO getComments(Long itemsId, Pageable pageable, boolean includeReply);

    Optional<CommentRating> getRatingOfComments(Long itemsId);

    CommentRating get(Long itemsId);
}
