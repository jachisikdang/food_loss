package com.foodloss.v1.items.base.model;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Developer : 김태현
 * Date : 08/10/2019
 */
@Getter
@Setter
@NoArgsConstructor
public class ItemDTO {

    private Long idx;

    private String name;

    private String description;

    private double price;

    private String shopName;

    private int discountRate;   // 할인율

    private List<String> images;

    private boolean isSell;

    private long reviews;

    private double rating;

}
