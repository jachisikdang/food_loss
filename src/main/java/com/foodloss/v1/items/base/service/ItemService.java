package com.foodloss.v1.items.base.service;

import java.util.List;
import com.foodloss.v1.items.base.model.Item;
import com.foodloss.v1.items.base.model.ItemDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
public interface ItemService {

    void saveItem(Item item);

    List<ItemDTO> findItems();

    Page<ItemDTO> findItems(Pageable pageable);

    ItemDTO findOne(Long itemId);

    long countOfItems();
}
