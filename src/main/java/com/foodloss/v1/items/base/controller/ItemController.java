package com.foodloss.v1.items.base.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import com.foodloss.common.api.ApiResponse;
import com.foodloss.common.api.ApiResponseFactory;
import com.foodloss.v1.items.base.model.Food;
import com.foodloss.v1.items.base.model.Item;
import com.foodloss.v1.items.base.model.ItemDTO;
import com.foodloss.v1.items.base.service.ItemService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping(("/v1/items"))
public class ItemController {

    private final ItemService itemService;


    // 상품 전체 조회
    @GetMapping
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        List<ItemDTO> items = itemService.findItems();

        return ApiResponseFactory
                .createSuccessResponse(request.getRequestURI(), "상품 전체 조회 성공", items, items.size());

    }


    // 상품 단일 조회
    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getItem(@PathVariable(name = "id") Long id,
            HttpServletRequest request) {
        log.info("/v1/items/" + id);

        ItemDTO item = itemService.findOne(id);

        return ApiResponseFactory
                .createSuccessResponse(request.getRequestURI(), "상품 조회 성공", item, 1);
    }


    // 상품 등록
    @PostMapping
    public ResponseEntity<ApiResponse> create(Food food, HttpServletRequest request) {
        log.info("/v1/items : create {}", food);

        itemService.saveItem(food);

        return ApiResponseFactory.createSuccessResponse(request.getRequestURI(), "상품 등록 성공");


    }


    // 상품 수정
    @PutMapping
    public ResponseEntity<ApiResponse> edit(Item item, HttpServletRequest request) {
        itemService.saveItem(item);

        return ApiResponseFactory.createSuccessResponse(request.getRequestURI(), "상품 수정 성공");

    }


    // 등록된 상품 개수
    @GetMapping("/count")
    public ResponseEntity<ApiResponse> getTotalCountOfItem(HttpServletRequest request) {

        return ApiResponseFactory.createSuccessResponse(request.getRequestURI(), "상품 개수 조회 성공",
                itemService.countOfItems());
    }

}
