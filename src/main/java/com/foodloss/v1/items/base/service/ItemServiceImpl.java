package com.foodloss.v1.items.base.service;

import java.util.List;
import java.util.stream.Collectors;
import com.foodloss.v1.items.base.model.Item;
import com.foodloss.v1.items.base.model.ItemDTO;
import com.foodloss.v1.items.base.repository.ItemRepository;
import com.foodloss.v1.items.comment.model.CommentRating;
import com.foodloss.v1.items.comment.service.CommentService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
@Service
@AllArgsConstructor
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;

    private final ModelMapper modelMapper;

    private final CommentService commentService;


    @Override
    public void saveItem(Item item) {
        itemRepository.save(item);
    }


    @Override
    public List<ItemDTO> findItems() {
        List<Item> items = itemRepository.findAll();

        return items.stream().map(item -> modelMapper.map(item, ItemDTO.class))
                .collect(Collectors.toList());
    }


    @Override
    public Page<ItemDTO> findItems(Pageable pageable) {
        Page<Item> itemsPage = itemRepository.findAllByIsSell(true, pageable);
        return itemsPage.map(item -> modelMapper.map(item, ItemDTO.class));
    }


    @Override
    public ItemDTO findOne(Long itemId) {
        ItemDTO itemDTO = modelMapper.map(itemRepository.findByIdx(itemId), ItemDTO.class);

        //        Optional<CommentRating> optionalCommentRating = commentService.getRatingOfComments(itemId);
        //
        //        if(optionalCommentRating.isPresent()) {
        //            itemDTO.setRating(optionalCommentRating.get().getRating());
        //            itemDTO.setReviews(optionalCommentRating.get().getNumOfComment());
        //        }

        CommentRating commentRating = commentService.get(itemId);
        return itemDTO;
    }


    @Override
    public long countOfItems() {
        return itemRepository.count();
    }
}
