package com.foodloss.v1.items.base.model;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
public class NotEnoughStockException extends RuntimeException {

    public NotEnoughStockException(String message) {
        super(message);
    }
}
