package com.foodloss.v1.items.base.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import com.foodloss.common.converter.StringListConverter;
import lombok.Getter;
import lombok.Setter;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
@Entity
@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn
public abstract class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "item_id")
    private Long idx;

    private String name;        //이름
    private int price;          //가격
    private int stockQuantity;  //재고수량

    @Column(name = "discount_rate")
    private int discountRate;   // 할인율

    @Column(name = "is_sell")
    private boolean isSell;

    @Convert(converter = StringListConverter.class)
    @Column(name = "image_url")
    private List<String> images;

    private String description;

    private String shopName;


    public void addStock(int orderQuantity) {
        this.stockQuantity += orderQuantity;
    }


    public void removeStock(int orderQuantity) {

        int restStock = this.stockQuantity - orderQuantity;
        if(restStock < 0)
            throw new NotEnoughStockException("need more stock");

        this.stockQuantity = restStock;
    }


    // 재고 부족 판단
    public boolean isOutOfStock() {
        return this.stockQuantity <= 0;
    }

}
