package com.foodloss.v1.items.base.repository;

import com.foodloss.v1.items.base.model.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
public interface ItemRepository extends JpaRepository<Item, Long> {

    Item findByIdx(Long itemId);

    Page<Item> findAllByIsSell(boolean isSell, Pageable pageable);
}
