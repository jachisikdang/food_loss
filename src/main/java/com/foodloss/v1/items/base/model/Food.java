package com.foodloss.v1.items.base.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Developer : 김태현
 * Date : 04/10/2019
 */
@Entity
@DiscriminatorValue("F")
@Getter
@Setter
@ToString
public class Food extends Item {

    private String shopName;
}
