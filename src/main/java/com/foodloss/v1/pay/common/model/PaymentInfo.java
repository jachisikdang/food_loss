package com.foodloss.v1.pay.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 결제 요청에 사용되는 기본 정보
 * Developer : 김태현
 * Date : 2019-08-21
 */
@Getter
@Setter
@ToString
public class PaymentInfo {

    @JsonProperty("order_id")
    private String orderId;

    private int amount;
}
