package com.foodloss.v1.pay.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Developer : 김태현
 * Date : 18/11/2019
 */
@Getter
@Setter
@ToString
@Builder
public class PaymentResult {

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("status")
    private String status;
}
