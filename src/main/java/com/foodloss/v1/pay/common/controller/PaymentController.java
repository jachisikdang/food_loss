package com.foodloss.v1.pay.common.controller;

import com.foodloss.v1.order.request.model.entity.Order;
import com.foodloss.v1.order.request.service.paydone.OrderPayDoneService;
import com.foodloss.v1.order.search.service.OrderSearchService;
import com.foodloss.v1.pay.common.model.PaymentInfo;
import com.foodloss.v1.pay.common.service.validate.PaymentValidator;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Developer : 김태현
 * Date : 12/10/2019
 */
public abstract class PaymentController {

    @Autowired
    private OrderPayDoneService orderPayDoneService;

    @Autowired
    private OrderSearchService orderSearchService;

    @Autowired
    private PaymentValidator paymentValidator;


    protected abstract boolean validateParameters(PaymentInfo paymentInfo);


    protected boolean validatePrepareOrder(PaymentInfo paymentInfo) {
        return paymentValidator.validatePrepareOrder(paymentInfo);
    }


    protected void processAfterPaymentSuccess(String orderId) {

        orderPayDoneService.processAfterPaySuccess(orderId);
    }


    protected Order getOrder(String orderId) {
        return orderSearchService.findOrder(orderId);
    }
}
