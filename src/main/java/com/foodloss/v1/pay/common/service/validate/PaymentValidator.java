package com.foodloss.v1.pay.common.service.validate;

import com.foodloss.v1.pay.common.model.PaymentInfo;

/**
 * 결제 진행 전 검증
 * 1. 주문 상품 재고 다시 체크
 * 2. 멤버 포인트 체크 ( TODO )
 * <p>
 * Developer : 김태현
 * Date : 2019-08-21
 */
public interface PaymentValidator {

    boolean validatePrepareOrder(PaymentInfo paymentInfo);

    /**
     * 주문이 가능한지 판단 ( 재고 확인 )
     *
     * @param paymentInfo 결제 정보
     * @return 주문 가능 여부
     */
    boolean validateOrderAble(PaymentInfo paymentInfo);
}
