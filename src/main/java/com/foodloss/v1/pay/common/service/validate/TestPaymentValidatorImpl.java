package com.foodloss.v1.pay.common.service.validate;

import com.foodloss.v1.order.request.model.entity.Order;
import com.foodloss.v1.order.request.repository.OrderRepository;
import com.foodloss.v1.pay.common.model.PaymentInfo;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Developer : 김태현
 * Date : 2019-08-21
 */
@Service
@AllArgsConstructor
@Profile("!prod")
public class TestPaymentValidatorImpl implements PaymentValidator {

    private final OrderRepository orderRepository;


    @Override
    public boolean validatePrepareOrder(PaymentInfo paymentInfo) {
        return true;
    }


    @Override
    public boolean validateOrderAble(PaymentInfo paymentInfo) {
        Order order = orderRepository.findById(paymentInfo.getOrderId());

        if(order == null)
            throw new IllegalArgumentException("올바른 주문 번호가 아닙니다.");

        return order.getOrderItems().stream()
                .noneMatch(orderItem -> orderItem.getItem().isOutOfStock());

    }
}

