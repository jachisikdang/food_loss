package com.foodloss.v1.pay.kakaopay.service;

import java.io.IOException;
import java.math.BigDecimal;
import com.siot.IamportRestClient.IamportClient;
import com.siot.IamportRestClient.exception.IamportResponseException;
import com.siot.IamportRestClient.response.IamportResponse;
import com.siot.IamportRestClient.response.Payment;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Developer : 김태현
 * Date : 06/11/2019
 */
@Service
@AllArgsConstructor
public class KakaoPayServiceImpl implements KakaoPayService {

    private final IamportClient iamportClient;


    @Override
    public Payment getPayments(String iamportUid) throws IOException, IamportResponseException {
        IamportResponse<Payment> paymentIamportResponse = iamportClient.paymentByImpUid(iamportUid);
        return paymentIamportResponse.getResponse();
    }


    @Override
    public boolean isPaySuccess(Payment payment, BigDecimal amountToBePaid) {
        return payment.getStatus().equals("paid") && payment.getAmount().equals(amountToBePaid);
    }


}
