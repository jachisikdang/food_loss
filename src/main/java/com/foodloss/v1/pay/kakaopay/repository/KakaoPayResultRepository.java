package com.foodloss.v1.pay.kakaopay.repository;

import com.foodloss.v1.pay.kakaopay.model.KakaoPayResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 카카오페이 결과 정보 entity
 * <p>
 * Developer : 김태현
 * Date : 06/11/2019
 */
@Repository
public interface KakaoPayResultRepository extends JpaRepository<KakaoPayResult, Integer> {}
