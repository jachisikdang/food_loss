package com.foodloss.v1.pay.kakaopay.service;

import java.util.Objects;
import com.foodloss.v1.pay.kakaopay.converter.KakaoPayResultConverter;
import com.foodloss.v1.pay.kakaopay.model.KakaoPayResult;
import com.foodloss.v1.pay.kakaopay.repository.KakaoPayResultRepository;
import com.siot.IamportRestClient.response.Payment;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * Developer : 김태현
 * Date : 06/11/2019
 */
@Service
@AllArgsConstructor
public class KakaoPayDoneServiceImpl implements KakaoPayDoneService {

    private final KakaoPayResultRepository kakaoPayResultRepository;

    private final KakaoPayResultConverter kakaoPayResultConverter;


    @Override
    public void saveResult(Payment payment) {

        KakaoPayResult kakaoPayResult = kakaoPayResultConverter.convert(payment);

        Objects.requireNonNull(kakaoPayResult);

        kakaoPayResultRepository.save(kakaoPayResult);
    }
}
