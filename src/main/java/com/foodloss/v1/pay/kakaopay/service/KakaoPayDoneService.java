package com.foodloss.v1.pay.kakaopay.service;

import com.siot.IamportRestClient.response.Payment;

/**
 * 카카오 페이 결제가 끝난 후 동작 관련 인터페이스
 * Developer : 김태현
 * Date : 06/11/2019
 */
public interface KakaoPayDoneService {

    void saveResult(Payment payment);
}
