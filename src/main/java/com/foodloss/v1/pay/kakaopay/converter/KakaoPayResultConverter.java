package com.foodloss.v1.pay.kakaopay.converter;

import java.time.ZoneId;
import com.foodloss.v1.pay.kakaopay.model.KakaoPayResult;
import com.siot.IamportRestClient.response.Payment;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Developer : 김태현
 * Date : 06/11/2019
 */
@Component
public class KakaoPayResultConverter implements Converter<Payment, KakaoPayResult> {

    @Override
    public KakaoPayResult convert(Payment source) {

        return KakaoPayResult.builder().amount(source.getAmount()).apply_num(source.getApplyNum())
                .bank_code(source.getBankCode()).bank_name(source.getBankName())
                .buyer_name(source.getBuyerName()).buyer_tel(source.getBuyerTel())
                .cancel_amount(source.getCancelAmount()).cancel_reason(source.getCancelReason())
                .cancelled_at(source.getCancelledAt().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate())
                // cash_receipt_issued
                .cash_receipt_issued(source.isCashReceiptIssued()).escrow(source.isEscrow())
                .fail_reason(source.getFailReason())
                // failed_at
                .failed_at(source.getFailedAt().toInstant().atZone(ZoneId.systemDefault())
                        .toLocalDate()).imp_uid(source.getImpUid())
                .merchant_uid(source.getMerchantUid()).name(source.getName())
                // paid_at
                .paid_at(
                        source.getPaidAt().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .pay_method(source.getPayMethod()).pg_provider(source.getPgProvider())
                .pg_tid(source.getPgTid()).receipt_url(source.getReceiptUrl())
                .status(source.getStatus()).build();

    }
}
