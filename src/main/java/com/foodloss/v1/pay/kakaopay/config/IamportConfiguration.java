package com.foodloss.v1.pay.kakaopay.config;

import com.siot.IamportRestClient.IamportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Developer : 김태현
 * Date : 06/11/2019
 */
@Configuration
public class IamportConfiguration {

    @Value("${pay.iamport.api-key}")
    private String apiKey;

    @Value("${pay.iamport.api-secret}")
    private String apiSecret;


    @Bean
    public IamportClient iamportClient() {
        return new IamportClient(apiKey, apiSecret);
    }
}
