package com.foodloss.v1.pay.kakaopay.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import com.foodloss.common.api.ApiErrorCode;
import com.foodloss.common.api.ApiResponse;
import com.foodloss.common.api.ApiResponseFactory;
import com.foodloss.v1.auth.model.CurrentUser;
import com.foodloss.v1.auth.model.NanwoUserDetail;
import com.foodloss.v1.order.request.model.entity.Order;
import com.foodloss.v1.pay.common.controller.PaymentController;
import com.foodloss.v1.pay.common.model.PaymentInfo;
import com.foodloss.v1.pay.common.model.PaymentResult;
import com.foodloss.v1.pay.kakaopay.model.request.KakaoPaymentInfo;
import com.foodloss.v1.pay.kakaopay.service.KakaoPayDoneService;
import com.foodloss.v1.pay.kakaopay.service.KakaoPayService;
import com.siot.IamportRestClient.exception.IamportResponseException;
import com.siot.IamportRestClient.response.Payment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Developer : 김태현
 * Date : 05/11/2019
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/kakaopay")
@Slf4j
public class KakaoPayController extends PaymentController {

    private final KakaoPayService kakaoPayService;
    private final KakaoPayDoneService kakaoPayDoneService;

    @Value("${nanwo.url.web")
    private String nanwoWebUrl;


    /**
     * @param kakaoPaymentInfo
     * @param nanwoUserDetail
     * @param request
     * @return
     * @throws IOException
     * @throws IamportResponseException
     */
    @PostMapping(value = "/payments/complete")
    public ResponseEntity<ApiResponse> paymentsComplete(
            @RequestBody KakaoPaymentInfo kakaoPaymentInfo,
            @CurrentUser NanwoUserDetail nanwoUserDetail, HttpServletRequest request)
            throws IOException, IamportResponseException {

        log.info("카카오 주문 : kakaoPaymentInfo - {}", kakaoPaymentInfo);
        // 1. iamport_uid 기준 결제정보 조회
        Payment payment = kakaoPayService.getPayments(kakaoPaymentInfo.getIamportUid());

        // 2. 결제될 총액 계산을 위해 Order 를 가져옴
        Order order = getOrder(kakaoPaymentInfo.getOrderId());

        // 3. 결제 결과 확인 ( iamport <-> nanwo )
        if(kakaoPayService.isPaySuccess(payment, order.getTotalPrice())) {

            processAfterPaymentSuccess(kakaoPaymentInfo.getOrderId());

            kakaoPayDoneService.saveResult(payment);

            return ApiResponseFactory
                    .createSuccessResponse(request.getRequestURI(), "iamport 결제 성공",
                            PaymentResult.builder().orderId(kakaoPaymentInfo.getOrderId())
                                    .status("success").build());
        } else
            return ApiResponseFactory.createFailResponse(request.getRequestURI(),
                    ApiErrorCode.PAY_IAMPORT_PAYMENT_FAIL, "import 결제 실패(위조된 결제시도)",
                    HttpStatus.BAD_REQUEST);
    }


    @Override
    protected boolean validateParameters(PaymentInfo paymentInfo) {
        return true;
    }

}
