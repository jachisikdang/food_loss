package com.foodloss.v1.pay.kakaopay.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.foodloss.v1.pay.common.model.PaymentInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Developer : 김태현
 * Date : 06/11/2019
 */
@Getter
@Setter
@ToString
public class KakaoPaymentInfo extends PaymentInfo {

    @JsonProperty("imp_uid")
    private String iamportUid;

    @JsonProperty("merchant_uid")
    private String merchantUid;

}
