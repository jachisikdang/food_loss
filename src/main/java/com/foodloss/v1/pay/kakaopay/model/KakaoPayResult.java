package com.foodloss.v1.pay.kakaopay.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Developer : 김태현
 * Date : 06/11/2019
 */
@Entity
@Table(name = "kakaopay_result")
@Getter
@Setter
@Builder
public class KakaoPayResult {

    @Column(name = "escrow")
    boolean escrow;
    @Column(name = "amount")
    BigDecimal amount;
    @Column(name = "cancel_amount")
    BigDecimal cancel_amount;
    @Column(name = "paid_at")
    LocalDate paid_at;
    @Column(name = "failed_at")
    LocalDate failed_at;
    @Column(name = "cancelled_at")
    LocalDate cancelled_at;
    @Column(name = "cash_receipt_issued")
    boolean cash_receipt_issued;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "item_id")
    private int id;
    @Column(name = "imp_uid")
    private String imp_uid;
    @Column(name = "merchant_uid")
    private String merchant_uid;
    @Column(name = "pay_method")
    private String pay_method;
    @Column(name = "pg_provider")
    private String pg_provider;
    @Column(name = "pg_tid")
    private String pg_tid;
    @Column(name = "apply_num")
    private String apply_num;
    @Column(name = "bank_code")
    private String bank_code;
    @Column(name = "bank_name")
    private String bank_name;
    @Column(name = "name")
    private String name;
    @Column(name = "buyer_name")
    private String buyer_name;
    @Column(name = "buyer_tel")
    private String buyer_tel;
    @Column(name = "status")
    private String status;
    @Column(name = "fail_reason")
    private String fail_reason;
    @Column(name = "cancel_reason")
    private String cancel_reason;
    @Column(name = "receipt_url")
    private String receipt_url;
}
