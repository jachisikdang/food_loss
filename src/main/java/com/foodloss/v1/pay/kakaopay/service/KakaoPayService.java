package com.foodloss.v1.pay.kakaopay.service;

import java.io.IOException;
import java.math.BigDecimal;
import com.siot.IamportRestClient.exception.IamportResponseException;
import com.siot.IamportRestClient.response.Payment;

/**
 * Developer : 김태현
 * Date : 06/11/2019
 */
public interface KakaoPayService {

    /**
     * 아임포트 공유번호로 결제내역 확인
     *
     * @param iamportUid 아임포트 고유번호
     * @return
     */
    Payment getPayments(String iamportUid) throws IOException, IamportResponseException;

    /**
     * 실제 결제가 최종적으로 완료 되었는지
     * 원하는 금액만큼 결제가 되었는지 확인
     *
     * @param payment        iamport 결제 후 응답
     * @param amountToBePaid 원하는 금액
     * @return 결제가 제대로 됐는지
     */
    boolean isPaySuccess(Payment payment, BigDecimal amountToBePaid);

}
