package com.foodloss.v1.auth.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.foodloss.v1.auth.model.AuthenticationResponse;
import com.foodloss.v1.auth.model.LoginRequest;

/**
 * Developer : 김태현
 * Date : 14/11/2019
 */
public interface LoginSuccessHandler {

    AuthenticationResponse loginSucceed(LoginRequest loginRequest, HttpServletRequest request,
            HttpServletResponse response);
}
