package com.foodloss.v1.auth.handler;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.foodloss.common.util.CookieUtil;
import com.foodloss.v1.auth.model.AuthenticationResponse;
import com.foodloss.v1.auth.model.LoginRequest;
import com.foodloss.v1.auth.model.NanwoUserDetail;
import com.foodloss.v1.auth.service.NanwoUserDetailService;
import com.foodloss.v1.auth.util.JwtTokenUtil;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * Developer : 김태현
 * Date : 14/11/2019
 */
@Service
@AllArgsConstructor
public class LoginSuccessHandlerImpl implements LoginSuccessHandler {

    private final NanwoUserDetailService userDetailsService;

    private final JwtTokenUtil jwtTokenUtil;


    @Override
    public AuthenticationResponse loginSucceed(LoginRequest loginRequest,
            HttpServletRequest request, HttpServletResponse response) {
        // 1. get user of 나눠먹자
        UserDetails userDetails = userDetailsService.loadUserByUsername(loginRequest.getUsername());

        // 2. generate JWT(Json Web Token) & name
        final String token = jwtTokenUtil.generateToken(userDetails);
        final String name = ((NanwoUserDetail) userDetails).getMember().getName();

        // 3. set cookie
        setCookie(request, response, token);

        return AuthenticationResponse.builder().token(token).name(name).build();

    }


    private void setCookie(HttpServletRequest request, HttpServletResponse response, String token) {
        Cookie cookie = new Cookie("login", token);
        cookie.setMaxAge(3600);
        cookie.setPath(CookieUtil.getCookiePath(request));
        response.addCookie(cookie);
    }
}
