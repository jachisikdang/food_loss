package com.foodloss.v1.auth.repository;

import java.util.Optional;
import com.foodloss.v1.member.model.Member;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Developer : 김태현
 * Date : 05/10/2019
 */
public interface UserDetailRepository extends JpaRepository<Member, Long> {

    Optional<Member> findByUsername(String username);
}
