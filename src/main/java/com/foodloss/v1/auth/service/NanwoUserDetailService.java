package com.foodloss.v1.auth.service;

import java.util.Optional;
import com.foodloss.v1.auth.model.NanwoUserDetail;
import com.foodloss.v1.auth.repository.UserDetailRepository;
import com.foodloss.v1.member.model.Member;
import com.foodloss.v1.member.model.MemberDTO;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Developer : 김태현
 * Date : 05/10/2019
 */
@Service
@AllArgsConstructor
public class NanwoUserDetailService implements UserDetailsService {

    private final UserDetailRepository userDetailRepository;
    private final ModelMapper modelMapper;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<Member> optionalMember = userDetailRepository.findByUsername(username);

        if(optionalMember.isEmpty())
            throw new UsernameNotFoundException(username + " is not found");

        return new NanwoUserDetail(modelMapper.map(optionalMember.get(), MemberDTO.class));
    }
}
