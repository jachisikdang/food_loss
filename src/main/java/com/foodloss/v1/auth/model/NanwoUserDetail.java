package com.foodloss.v1.auth.model;

import java.io.Serializable;
import java.util.Collection;
import com.foodloss.v1.member.model.MemberDTO;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Developer : 김태현
 * Date : 05/10/2019
 */
@Getter
@Setter
@ToString
public class NanwoUserDetail implements UserDetails, Serializable {

    private static final long serialVersionUID = 1905122041950251207L;

    private MemberDTO member;


    public NanwoUserDetail(MemberDTO member) {
        this.member = member;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Lists.newArrayList();
    }


    @Override
    public String getPassword() {
        return member.getPassword();
    }


    @Override
    public String getUsername() {
        return member.getUsername();
    }


    @Override
    public boolean isAccountNonExpired() {
        return true;
    }


    @Override
    public boolean isAccountNonLocked() {
        return true;
    }


    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }


    @Override
    public boolean isEnabled() {
        return true;
    }
}
