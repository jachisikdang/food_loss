package com.foodloss.v1.auth.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Developer : 김태현
 * Date : 02/10/2019
 */
@Getter
@Setter
@Builder
@ToString
public class AuthenticationResponse {

    private String token;

    private String name;
}
