package com.foodloss.v1.auth.controller;

import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.foodloss.common.api.ApiResponseFactory;
import com.foodloss.v1.auth.handler.LoginSuccessHandler;
import com.foodloss.v1.auth.model.AuthenticationException;
import com.foodloss.v1.auth.model.AuthenticationResponse;
import com.foodloss.v1.auth.model.LoginRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Developer : 김태현
 * Date : 05/10/2019
 */
@RestController
@AllArgsConstructor
public class LoginController {

    private final AuthenticationManager authenticationManager;
    private final LoginSuccessHandler loginSuccessHandler;


    @PostMapping(value = "/v1/authenticate")
    public ResponseEntity createAuthenticationToken(@RequestBody LoginRequest authenticationRequest,
            HttpServletRequest request, HttpServletResponse response) {

        // 1. authenticate user
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        // 2. 로그인 성공 후 처리
        AuthenticationResponse result =
                loginSuccessHandler.loginSucceed(authenticationRequest, request, response);


        // 3. return response
        return ApiResponseFactory.createSuccessResponse(request.getRequestURI(), "토큰 발급", result);
    }


    private void authenticate(String username, String password) {

        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch(DisabledException e) {
            throw new AuthenticationException("USER_DISABLED", e);
        } catch(BadCredentialsException e) {
            throw new AuthenticationException("INVALID_CREDENTIALS", e);
        }
    }


}
