package com.foodloss.v1.basket;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

/**
 * 장바구니 관련 API
 * Developer : 김태현
 * Date : 2019-08-17
 */
@RestController
@AllArgsConstructor
@Slf4j
public class BasketController {}
