package com.foodloss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodlossApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodlossApplication.class, args);
    }

}
