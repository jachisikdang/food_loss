FROM azul/zulu-openjdk:11
ADD build/libs/nanwomukja-0.1.0.jar app.jar

ENTRYPOINT java -jar -Dspring.profiles.active=prod app.jar
